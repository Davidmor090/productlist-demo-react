import React from 'react';

class ProductCard extends React.Component {
  render() {
    const {name, category, price, features, img} = this.props.product;
        return (
          <div>
            <div className="productcard">
              <img src={img} alt=""/>
              <h2>{category}</h2>
              <h1>{name}</h1>
              <ul>
                {features.map(feature => {
                  return (<li>{feature}</li>)
                })}
              </ul>
              <h3>price: {price}€</h3>
            </div>
          </div>
        )
    }

}

export default ProductCard;
