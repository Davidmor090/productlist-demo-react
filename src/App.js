import React from 'react';
import './App.scss';
import ProductCard from './components/ProductCard';

class App extends React.Component {

  constructor(props) {
        super(props);

        this.state = {
          productlist: [
            {name: 'product1',
            category: 'category1',
            features: ['feature2', 'feature4'],
            price: 45,
            img: "https://i.picsum.photos/id/1/100/100.jpg"},
            {name: 'product2',
            category: 'category1',
            features: ['feature1', 'feature2'],
            price: 66,
            img: "https://i.picsum.photos/id/2/100/100.jpg"},
            {name: 'product3',
            category: 'category2',
            features: ['feature1','feature2', 'feature4'],
            price: 49,
            img: "https://i.picsum.photos/id/3/100/100.jpg"},
            {name: 'product4',
            category: 'category2',
            features: ['feature3', 'feature4'],
            price: 12,
            img: "https://i.picsum.photos/id/4/100/100.jpg"},
            {name: 'product5',
            category: 'category3',
            features: ['feature4'],
            price: 77,
            img: "https://i.picsum.photos/id/5/100/100.jpg"},
            {name: 'product6',
            category: 'category2',
            features: ['feature2','feature3', 'feature4'],
            price: 50,
            img: "https://i.picsum.photos/id/6/100/100.jpg"},
            {name: 'product7',
            category: 'category3',
            features: ['feature1'],
            price: 20,
            img: "https://i.picsum.photos/id/7/100/100.jpg"}

          ],
          filteredProductList: [],
          categoryfilter: "all",
          pricefilter: 0,
          featurefilter: ['feature1', 'feature2', 'feature3', 'feature4'],
          sortbyprice: 'asc'

        };
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeRadio = this.handleChangeRadio.bind(this);

    this.filterProducts = function() {
      let tempProducts = [];
      this.state.filteredProductList = [];
      console.log("change")
      this.state.productlist.forEach(product => {
        if(product.price > this.state.pricefilter && (product.category === this.state.categoryfilter || this.state.categoryfilter === "all")) {
      if (this.haveFeatures(product) === true) {
        console.log(product.name, " included")
      const singleItem = {...product};
      tempProducts = [...tempProducts, singleItem];
      }

        }

        this.state.filteredProductList = tempProducts;
      })
      this.sortedProducts();
    }

    this.haveFeatures = function(product) {
      let result = false;
      for (let i = 0; i <= this.state.featurefilter.length; i++) {
        if(product.features.includes(this.state.featurefilter[i-1])) {
          result = true;
        }
      }
      return result;
    }

    this.sortedProducts = function() {
      if ( this.state.sortbyprice === "asc") {
        this.state.filteredProductList.sort((a, b) => (a.price > b.price) ? 1 : -1)
      }
      else {
        this.state.filteredProductList.sort((a, b) => (a.price > b.price) ? -1 : 1)
      }
    }
  }
    handleChange(e) {
      if (e.target.name === 'featurefilter') {
        let templist = this.state.featurefilter;
      if         (this.state.featurefilter.includes(e.target.value)) {
        templist = templist.filter(feature => { return feature !== e.target.value})
      }
        else {
          templist.push(e.target.value)
        }
        this.setState(()=>{
      return {
        featurefilter: [...templist] }
      })
        console.log(templist)
        console.log(this.state.featurefilter)
      }
      else {
        this.setState({
        [e.target.name]: e.target.value
      });
      }
    }

  handleChangeRadio(e) {
    console.log(e.target.value)
    this.setState({
      categoryfilter: e.target.value
    })
  }

  render() {
    this.filterProducts()
        return (

          <div class="container">
            <div class="filters">
              <details>
                <summary>categories</summary>
                <section>
                  <input onChange={this.handleChangeRadio} name="category" checked={this.state.categoryfilter === "all"} value="all" type="radio"/>
                  <label>show all</label>
                </section>
                <section>
                  <input checked={this.state.categoryfilter === "category1"} onChange={this.handleChangeRadio} name="category" value="category1" type="radio"/>
                  <label>category1</label>
                </section>
                <section>
                  <input checked={this.state.categoryfilter === "category2"} onChange={this.handleChangeRadio} name="category" value="category2" type="radio"/>
                  <label>category2</label>
                </section>
                <section>
                  <input checked={this.state.categoryfilter === "category3"} onChange={this.handleChangeRadio} name="category" value="category3" type="radio"/>
                  <label>category3</label>
                </section>
              </details>
              <hr/>
              <details>
                <summary>features</summary>
                <section>
                  <input onChange={this.handleChange} name="featurefilter" checked={this.state.featurefilter.includes("feature1")} value="feature1" type="checkbox"/>
                  <label>feature1</label>
                </section>
                <section>
                  <input onChange={this.handleChange} name="featurefilter" checked={this.state.featurefilter.includes("feature2")}  value="feature2" type="checkbox"/>
                  <label>feature2</label>
                </section>
                <section>
                  <input onChange={this.handleChange} name="featurefilter" checked={this.state.featurefilter.includes("feature3")} value="feature3" type="checkbox"/>
                  <label>feature3</label>
                </section>
                <section>
                  <input onChange={this.handleChange} name="featurefilter" checked={this.state.featurefilter.includes("feature4")}  value="feature4" type="checkbox"/>
                  <label>feature4</label>
                </section>
              </details>
              <hr/>
              <details>
                <summary>min price</summary>
                <input name="pricefilter" onChange={this.handleChange} title={this.state.pricefilter} type="range" min="0" max="100" value={this.state.pricefilter}/><label><span>{this.state.pricefilter}</span></label>
              </details>
              <hr/>
              <details>
                <summary>sort by price: </summary>
                <select onChange={this.handleChange} name="sortbyprice">
                  <option value="asc">asc</option>
                  <option value="desc">desc</option>
                </select>
              </details>
            </div>
                <div class="productlist"> {this.state.filteredProductList.map(product => {
                return (
                  <ProductCard product={product} />)
              })}
              </div>
          </div>
        )
    }
}

export default App;
